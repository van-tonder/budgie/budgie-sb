package dev.vantonder.budgie

import io.kotest.core.spec.style.WordSpec
import io.kotest.matchers.shouldBe

class SampleUnitTest : WordSpec({
  "String equality" should {
    "hold" {
      "1" shouldBe "1"
    }
  }
})
