package dev.vantonder.budgie

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BudgieApplication

fun main(args: Array<String>) {
	runApplication<BudgieApplication>(*args)
}
