pluginManagement {
  repositories {
    jcenter()
    gradlePluginPortal()
  }
}

rootProject.name = "budgie"
