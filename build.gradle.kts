import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("jacoco")
  id("org.sonarqube") version "3.1.1"
  id("org.jlleitschuh.gradle.ktlint") version "9.4.1"
  id("io.spring.dependency-management") version "1.0.11.RELEASE"
  id("org.asciidoctor.convert") version "1.5.8"
  id("org.springframework.boot") version "2.4.2"
  kotlin("jvm") version "1.4.21"
  kotlin("kapt") version "1.4.21"
  kotlin("plugin.spring") version "1.4.21"
}

group = "dev.vantonder"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

repositories {
	jcenter()
}

extra["kotestVersion"] = "4.3.2"
extra["mockkVersion"] = "1.10.5"

extra["snippetsDir"] = file("build/generated-snippets")

dependencies {
  developmentOnly("org.springframework.boot:spring-boot-devtools")
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-data-rest")
  implementation("org.springframework.boot:spring-boot-starter-integration")
  implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("org.springframework.integration:spring-integration-http")
  kapt("org.springframework.boot:spring-boot-configuration-processor")
  runtimeOnly("org.postgresql:postgresql")
  testImplementation("io.mockk:mockk:${ property("mockkVersion") }")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.springframework.integration:spring-integration-test")
  property("kotestVersion").also { version ->
    testImplementation("io.kotest:kotest-runner-junit5:$version")
    testImplementation("io.kotest:kotest-assertions-core:$version")
    testImplementation("io.kotest:kotest-property:$version")
    testImplementation("io.kotest:kotest-extensions-spring:$version")
  }
}

sonarqube {
  properties {
    property("sonar.projectKey", "van-tonder_budgie-sb")
    property("sonar.organization", "van-tonder")
  }
}

sourceSets.create("e2e") {
  withConvention(org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet::class) {
    kotlin.srcDir("src/e2e/kotlin")
    resources.srcDir("src/e2e/resources")
    compileClasspath += sourceSets["main"].output + configurations["testRuntimeClasspath"]
    runtimeClasspath += output + compileClasspath + sourceSets["test"].runtimeClasspath
  }
}

val e2e = task<Test>("e2e") {
  val snippetsDir = project.property("snippetsDir") as File
  description = "Runs the e2e tests."
  group = "verification"
  testClassesDirs = sourceSets["e2e"].output.classesDirs
  classpath = sourceSets["e2e"].runtimeClasspath
  outputs.dir(snippetsDir)
  shouldRunAfter(tasks.test)
}

springBoot {
  buildInfo()
}

tasks.asciidoctor {
  val snippetsDir = project.property("snippetsDir") as File
  inputs.dir(snippetsDir)
  attributes(
      mapOf(
          "project-version" to project.version
      )
  )
}

tasks.bootBuildImage {
  imageName = projectDockerImageName()
}

tasks.build {
  dependsOn(e2e)
  finalizedBy(tasks.asciidoctor, tasks.jacocoTestReport)
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "11"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}

tasks.jacocoTestReport {
  executionData.setFrom(fileTree(buildDir).include("jacoco/*.exec"))
  reports {
    xml.isEnabled = true
  }
}

fun projectDockerImageName(): String {
  val dockerRepo = System.getenv("DOCKER_REPOSITORY") ?: "registry.gitlab.com/van-tonder/budgie-sb"
  val dockerArtifact = System.getenv("DOCKER_ARTIFACT") ?: project.name
  val dockerTag = System.getenv("DOCKER_ARTIFACT_TAG") ?: run {
    val releaseVersion = System.getenv("RELEASE_VERSION") ?: project.version.toString()
    val versionTagPair = releaseVersion.toLowerCase().split("-")
    val removeNumberFromTag = { versionTagPair[1].replace("[^a-z]".toRegex(), "") }
    when {
      versionTagPair.size >= 2 -> removeNumberFromTag()
      else -> "latest"
    }
  }
  return "$dockerRepo/$dockerArtifact:$dockerTag"
}
