## Coding rules

### Source code

To ensure consistency and quality throughout the source code, all code modifications must have:
- No [linting](#lint) errors
- A [test](#tests) for every possible case introduced by your code change
  *(an ideal - not strictly enforced to avoid meaningless tests)*
- **100%** test coverage *(an ideal - not strictly enforced to avoid meaningless tests)*
- [Valid commit message(s)](#commit-message-guidelines)
- Documentation for new features
- Updated documentation for modified features

### Branching policy and release workflow

This project uses [semantic-release](https://semantic-release.gitbook.io/semantic-release/) for
automated version management, which also implies the use of
[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) (see
[Commit message guidelines](#commit-message-guidelines)).

In order for the automated version management to work correctly, and smoothly, please read the
[workflow recipes](https://semantic-release.gitbook.io/semantic-release/recipes/recipes#release-workflow)
provided by **semantic-release** to gain an understanding of the workflow, and branching policy,
applied to this project.

Specifically take note of *Publishing on distribution channels*, and *Publishing pre-releases*.

#### Branches

| Name      | Type          | Description                                                       |
| ----      | ----          | -----------                                                       |
| master    | release       | Latest, publicly available version of the codebase                |
| rc        | pre-release   | Release candidate undergoing QA                                   |
| beta      | pre-release   | A version of the codebase that is undergoing minimal development  |
| alpha     | pre-release   | A version of the codebase that is undergoing major development    |

In order to commit changes, one should:
1. Checkout a short-lived
   [feature branch](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
2. Make changes (with tests)
3. Ensure tests pass
4. Commit changes using [commit message guidelines](#commit-message-guidelines)
5. Create merge request for desired release, or pre-release branch
6. Participate in review process

### Commit message guidelines

#### Atomic commits

If possible, make [atomic commits](https://en.wikipedia.org/wiki/Atomic_commit), which means:
- a commit should contain exactly one self-contained functional change
- a functional change should be contained in exactly one commit
- a commit should not create an inconsistent state (such as test errors, linting errors,
  partial fix, feature with documentation etc...)

A complex feature can be broken down into multiple commits as long as each one maintains a
consistent state and consists of a self-contained change.

#### Commit message format

Each commit message consists of a **header**, a **body** and a **footer**.
The header has a special format that includes a **type**, a **scope**, and a **subject**:

```commit
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory, and the **scope** of the header is optional.

The **footer** can contain a
[closing reference to an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).

#### Revert

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of
the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the
SHA of the commit being reverted.

#### Type

The type must be one of the following:

| Type         | Description                                                                                                 |
|--------------|-------------------------------------------------------------------------------------------------------------|
| **build**    | Changes that affect the build system or external dependencies (example scopes: gulp, npm, gradle)           |
| **ci**       | Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) |
| **docs**     | Documentation only changes                                                                                  |
| **feat**     | A new feature                                                                                               |
| **fix**      | A bug fix                                                                                                   |
| **perf**     | A code change that improves performance                                                                     |
| **refactor** | A code change that neither fixes a bug nor adds a feature                                                   |
| **style**    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)      |
| **test**     | Adding missing tests or correcting existing tests                                                           |

#### Subject

The subject contains succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize first letter
- no dot (.) at the end

#### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

#### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference GitHub issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines.
The rest of the commit message is then used for this.

#### Examples

```commit
fix(pencil): stop graphite breaking when too much pressure applied
```

```commit
feat(pencil): add 'graphiteWidth' option

Fix #42
```

```commit
perf(pencil): remove graphiteWidth option

BREAKING CHANGE: The graphiteWidth option has been removed.

The default graphite width of 10mm is always used for performance reasons.
```

## Working with the code

### Set up the workspace

[Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
the project:

```shell script
# Clone the repo into the current directory
$ git clone <repo url>
# Navigate to the newly cloned directory
$ cd <project>
# [Optional] Run checks (will also download dependencies and verify that project works)
$ ./gradlew check
```

### Lint

This project uses [ktlint](https://ktlint.github.io) for linting of Kotlin code.

Before pushing your code changes make sure there are no linting errors with `./gradlew ktlintCheck`.

> **_NOTE_**: `./gradlew ktlintCheck` will only run ktlint.
>
> `./gradlew check` will run ktlint, as well as any other verification tasks, such as unit tests.

> **_NOTE_**: To fix some (not all) linting issues, one can run the `./gradlew ktlintFormat` task.

### Tests

This project makes an explicit distinction between unit tests and integration/e2e tests.

Each module has its own suite of unit tests within their respective `test` source-sets.

The `bootstrap` module houses the integration/e2e tests of the project, within its `e2e` source-set.

Before pushing your code changes make sure all **tests pass**:

```shell script
$ ./gradlew build
```

> **_NOTE_**: `./gradlew build` will compile, assemble, package, and lint the project, as well as
> run both unit tests and integration/e2e tests.
>
> `./gradlew test` can be used to run only the unit tests, while `./gradlew e2e` can be used to run
> only the integration/e2e tests.
>
> It should also be noted that one can run only the unit tests of a particular module by specifying
> the full task path, e.g. to run the unit tests of the `<module>` module use
> `./gradlew :<path to module>:test`.
